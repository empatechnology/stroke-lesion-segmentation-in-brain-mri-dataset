'''
Empa Technolgy is an intrapreneur investment of Empa Elektronik, which is the market leader in semiconductor sector.
We develop and integrate technology for the growing IoT (Internet of Things) and AI (Artificial Intelligence) market.
Our goal is create a real impact by designing and implementing solid, high-scale,
value-adding IoT and Industry 4.0 solutions for customers who are eager to catch up with digital transformation benefits.
Contact us to learn more (teknoloji@empa.com)

Author : Arda Burak Mamur
Title : Computer Vision and Machine Learning Engineer
Latest update : 20.01.2022
'''

import tensorflow as tf
from tensorflow.keras.models import load_model
from src.functions import *
import argparse
import os
import cv2


class Test:

    def __init__(self,task):
        if task == "CLASSIFICATION":
            self.task = "cls"
        elif task == "SEGMENTATION":
            self.task = "seg"
        else:
            raise Exception("No task named as ", args.task)
        self.img_path = args.input
        self.model_path = args.model

    def save_segmented_img(self, all_images):
        mask = np.concatenate(all_images, axis=1)
        dir = "results/"
        cur_dir = os.getcwd()
        path = os.path.join(cur_dir, dir)
        if not os.path.exists(path):
            os.mkdir(path)

        results_dir = "results/" + self.task + "/"
        cur_dir = os.getcwd()
        path = os.path.join(cur_dir, results_dir)
        if not os.path.exists(path):
            os.mkdir(path)

        file_name = self.img_path.split(".")
        output_file = file_name[0]
        output_file = output_file + "_output.png"
        f_name = results_dir + output_file
        cv2.imwrite(f_name, mask)
        return


    def test(self):

        if self.task == "cls":
            model = load_model(self.model_path)
            img = read_img(self.img_path) # H,W set to 256x56 by default.
            img = img / 255.
            img = tf.expand_dims(img, axis=0)
            pred = model.predict(img)
            tmp = []
            tmp.append(pred[0][0])
            tmp.append(pred[0][1])
            ind = max(tmp)
            if ind == 0:
                print("NO TUMOR")
            else:
                print("TUMOR")

        else:
            model = load_model(self.model_path, compile=False)
            img = load_img(self.img_path, target_size=(256, 256) )# H,W set to 256x56 by default.
            mask_name = self.img_path.split(".")
            org_mask = mask_name[0]
            org_mask = org_mask + "_mask.tif"
            ground_truth = load_img(org_mask, target_size=(256, 256, 3))  # H,W set to 256x56 by default.
            x = np.zeros((1, 256, 256, 3), dtype="float32")
            x[0] = img
            x = x / 255
            pred = model.predict(x)
            line = np.ones((256, 10, 3)) * 255.0
            rounded_pred = np.where(pred >= 0.5, 1, 0)
            rounded_pred = rounded_pred.reshape((256,256))
            print(np.shape(rounded_pred))
            output_mask = convert_three_d(rounded_pred)
            print(np.shape(img))
            print(np.shape(line))
            print(np.shape(output_mask))
            all_images = [
                img,
                line,
                ground_truth,
                line,
                output_mask * 255.0
            ]
            self.save_segmented_img(all_images)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--task', help="Classification or Segmentation", type=str)
    parser.add_argument('--model', help="model path", type=str)
    parser.add_argument('--input', help="image path", type=str)
    args, _ = parser.parse_known_args()

    if args.task == "CLASSIFICATION" or args.task == "SEGMENTATION":
        main = Test(args.task)
        main.test()


    else:
        raise Exception("No task named as ", args.task)