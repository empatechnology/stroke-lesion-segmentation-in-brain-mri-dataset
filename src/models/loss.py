'''
Empa Technolgy is an intrapreneur investment of Empa Elektronik, which is the market leader in semiconductor sector.
We develop and integrate technology for the growing IoT (Internet of Things) and AI (Artificial Intelligence) market.
Our goal is create a real impact by designing and implementing solid, high-scale,
value-adding IoT and Industry 4.0 solutions for customers who are eager to catch up with digital transformation benefits.
Contact us to learn more (teknoloji@empa.com)
'''
import tensorflow as tf
from tensorflow.keras import backend as K

def dice_coefficient(y_true, y_pred, smooth=1e-7):
    '''
    dice coefficient calculated by using intersection of two different masks which are ground truth and predicted mask.
    :param y_true: ground truth
    :param y_pred: predicted mask
    :param smooth: bias
    :return:
    '''
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    score = (2. * intersection + smooth) / (
            K.sum(y_true_f) + K.sum(y_pred_f) + smooth)
    return score
def dice_loss(y_true, y_pred):
    loss = 1 - dice_coefficient(y_true, y_pred)
    return loss

def tversky(y_true, y_pred, smooth=1, alpha=0.7):
    '''
    :param y_true:
    :param y_pred:
    :param smooth:
    :param alpha:
    :return:
    '''
    y_true_pos = K.flatten(y_true)
    y_pred_pos = K.flatten(y_pred)
    true_pos = K.sum(y_true_pos * y_pred_pos)
    false_neg = K.sum(y_true_pos * (1 - y_pred_pos))
    false_pos = K.sum((1 - y_true_pos) * y_pred_pos)
    return (true_pos + smooth) / (true_pos + alpha * false_neg + (1 - alpha) * false_pos + smooth)
def tversky_loss(y_true, y_pred):
    return 1 - tversky(y_true, y_pred)

def focal_tversky_loss(y_true, y_pred, gamma=0.75):
    tv = tversky(y_true, y_pred)
    return K.pow((1 - tv), gamma)

def weighted_binary_crossentropy(y_true, y_pred):
    '''
    :param y_true:
    :param y_pred:
    :return:
    '''
    one_weight = 0.016
    zero_weight = 0.984
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    b_ce = K.binary_crossentropy(y_true_f, y_pred_f)
    weight_vector = y_true_f * one_weight + (1 - y_true_f) * zero_weight
    weighted_b_ce = weight_vector * b_ce
    loss = K.mean(weighted_b_ce)
    return loss
def pra_loss(y_mask: tf.Tensor, y_pred: tf.Tensor):
    bce_iou_weights = 1 + 5 * \
        tf.abs(tf.nn.avg_pool2d(y_mask, ksize=31,
               strides=1, padding="SAME")-y_mask)

    # weighted BCE loss
    bce_loss = tf.keras.losses.BinaryCrossentropy(
        from_logits=True)(y_mask, y_pred)
    wbce_loss = tf.reduce_sum(
        bce_loss*bce_iou_weights, axis=(1, 2)) / tf.reduce_sum(bce_iou_weights, axis=(1, 2))

    # weighted DICE loss
    y_pred = tf.sigmoid(y_pred)
    y_pred = tf.cast(tf.math.greater(y_pred, 0.5), tf.float32)

    inter = tf.reduce_sum((y_pred * y_mask) * bce_iou_weights, axis=(1, 2))
    union = tf.reduce_sum((y_pred + y_mask) * bce_iou_weights, axis=(1, 2))
    wdice_loss = 1 - ((2*inter) / union+1e-15)

    weighted_bce_dice_loss = tf.reduce_mean(
        wbce_loss + wdice_loss)
    return weighted_bce_dice_loss
