'''
Empa Technolgy is an intrapreneur investment of Empa Elektronik, which is the market leader in semiconductor sector.
We develop and integrate technology for the growing IoT (Internet of Things) and AI (Artificial Intelligence) market.
Our goal is create a real impact by designing and implementing solid, high-scale,
value-adding IoT and Industry 4.0 solutions for customers who are eager to catch up with digital transformation benefits.
Contact us to learn more (teknoloji@empa.com)

Author : Arda Burak Mamur
Title : Computer Vision and Machine Learning Engineer
Latest update : 17.01.2022
'''
import tensorflow.keras.utils
from tensorflow.keras.layers import Input, Dense, MaxPool2D, Conv2D, AvgPool2D, GlobalAveragePooling2D, BatchNormalization, ReLU, concatenate
import tensorflow.keras.backend as K
from tensorflow.keras.models import Model

def bn_rl_conv(x, filters, kernel=1, strides=1):
    x = BatchNormalization()(x)
    x = ReLU()(x)
    x = Conv2D(filters, kernel, strides=strides, padding='same')(x)
    return x

def dense_block(x, repetition, filters=32):
    for _ in range(repetition):
        y = bn_rl_conv(x, 4 *filters)
        y = bn_rl_conv(y, filters, 3)
        x = concatenate([y, x])
    return x


def transition_layer(x):
    x = bn_rl_conv(x, K.int_shape(x)[-1] // 2)
    x = AvgPool2D(2, strides=2, padding='same')(x)
    return x

def densenet_model(modelName, n_classes, height=512, width=512, channel_number=1):
    rept = []
    if modelName == "densenet121":
        rept = [6, 12, 24, 16]
    elif modelName == "densenet169":
        rept = [6, 12, 32, 32]
    elif modelName == "densenet201":
        rept = [6, 12, 48, 32]
    elif modelName == "densenet264":
        rept = [6, 12, 64, 48]
    else:
        raise Exception("No model named as ", modelName)

    shape = (height, width, channel_number)
    input = Input(shape)
    x = Conv2D(64, 7, strides=2, padding='same')(input)
    x = MaxPool2D(3, strides=2, padding='same')(x)
    for repetition in rept:
        d = dense_block(x, repetition)
        x = transition_layer(d)
    x = GlobalAveragePooling2D()(d)
    output = Dense(n_classes, activation='softmax')(x)
    model = Model(input, output)
    return model

if __name__ == '__main__':
    model = densenet_model(modelName='densenet121',n_classes=3)
    model.summary()
