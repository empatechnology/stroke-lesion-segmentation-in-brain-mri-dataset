'''
Empa Technolgy is an intrapreneur investment of Empa Elektronik, which is the market leader in semiconductor sector.
We develop and integrate technology for the growing IoT (Internet of Things) and AI (Artificial Intelligence) market.
Our goal is create a real impact by designing and implementing solid, high-scale,
value-adding IoT and Industry 4.0 solutions for customers who are eager to catch up with digital transformation benefits.
Contact us to learn more (teknoloji@empa.com)

Author : Arda Burak Mamur
Title : Computer Vision and Machine Learning Engineer
Latest update : 17.01.2022
'''
import numpy as np
import pandas as pd
from tqdm import tqdm
import os
from os.path import isfile, join
import tensorflow as tf
from sklearn.model_selection import train_test_split
from tensorflow.keras.preprocessing.image import load_img

def read_img(path,H,W):
    img = np.asarray(load_img(path, target_size=(H, W), color_mode="grayscale"))
    img = np.expand_dims(img, axis=-1)
    return img

def convert_two_d(gray_mask):
    two_d_mask = np.concatenate([gray_mask, gray_mask], axis=-1)
    return two_d_mask

def create_csv(data_folder, H=256, W=256):
    #data_folder = "../" + data_folder
    column_names = ["img_path", "mask_path", "label"]
    patients = os.listdir(data_folder)
    mask_images = []
    mri_images = []
    labels = []
    for patient in tqdm(patients):
        images = os.listdir(join(data_folder, patient))
        images.sort()
        for i in images:
            path = data_folder + "/" + patient + "/" + i
            if "mask" not in i:
                mri_images.append(path)
                f_name = patient + "/" + i
                abs_f_name = f_name.split(".")
                mask_name = abs_f_name[0] + "_mask.tif"
                mask_path = data_folder + "/" + mask_name
                mask = np.asarray(load_img(mask_path, target_size=(H, W), color_mode="grayscale"))
                mask_images.append(mask_path)
                if np.amax(mask) != 0:
                    labels.append(1)
                else:
                    labels.append(0)
    df_train = pd.DataFrame(columns=column_names)
    df_train['img_path'] = mri_images
    df_train['mask_path'] = mask_images
    df_train['label'] = labels
    df_train.to_csv("train.csv")


def classification_preprocess(csv_name="train.csv", H=256, W=256, test_size=0.1, val_size=0.1, batch_size=4):
    #print(os.getcwd())
    df_train = pd.read_csv(csv_name)
    img_list = df_train['img_path']
    labels = df_train['label']
    print("Total images with tumer :" , np.unique(labels, return_counts=True)[1][1])
    print("Total images without tumer :", np.unique(labels, return_counts=True)[1][0])
    images = []
    for i in img_list:
        img = read_img(i,H,W)
        img = img / 255.
        images.append(img)
    img_label = tf.keras.utils.to_categorical(labels)
    print("Image arr shape :", np.shape(images))
    print("Label arr len :", len(img_label))

    images = np.asarray(images).astype(np.float32)
    labels = np.asarray(img_label).astype(np.float32)
    train_tmp_img, test_img, train_tmp_labels, test_labels = train_test_split(images, labels, test_size=test_size,random_state=42)
    train_img, val_img, train_labels, val_labels = train_test_split(train_tmp_img, train_tmp_labels,test_size=val_size, random_state=42)

    number_train = len(train_labels)
    number_val = len(val_labels)

    train_steps = number_train // batch_size
    valid_steps = number_val // batch_size

    if number_train % batch_size != 0:
        train_steps += 1
    if number_val % batch_size != 0:
        valid_steps += 1

    return train_img, val_img, train_labels, val_labels, train_steps, valid_steps


def adjust_data(img, mask):
    img = img / 255
    mask = mask / 255
    mask[mask > 0.5] = 1
    mask[mask <= 0.5] = 0

    return (img, mask)


class BrainMRIs(tf.keras.utils.Sequence):

    def __init__(self, batch_size, H,W, input_img_paths, target_img_paths):
        self.batch_size = batch_size
        self.H = H
        self.W = W
        self.input_img_paths = input_img_paths
        self.target_img_paths = target_img_paths

    def __len__(self):
        return len(self.target_img_paths) // self.batch_size

    def __getitem__(self, idx):
        i = idx * self.batch_size
        batch_input_img_paths = self.input_img_paths[i: i + self.batch_size]
        batch_target_img_paths = self.target_img_paths[i: i + self.batch_size]

        x = np.zeros((self.batch_size,self.H, self.W, 3), dtype="float32")
        for j, path in enumerate(batch_input_img_paths):
            img = load_img(path, target_size=(self.H, self.W))
            x[j] = img

        y = np.zeros((self.batch_size, self.H, self.W, 1), dtype="float32")
        for j, path in enumerate(batch_target_img_paths):
            img = load_img(path, target_size=(self.H, self.W),
                           color_mode="grayscale")
            y[j] = np.expand_dims(img, 2)

        return adjust_data(x, y)

def segmentation_preprocess(csv_name="train.csv", H=256, W=256, val_size=0.1, num_test=20, batch_size=4):
    df_train = pd.read_csv(csv_name)
    df_seg = df_train.loc[df_train['label'] == 1]

    img_list = df_seg['img_path']
    mask_list = df_seg['mask_path']
    test_list = img_list[:num_test]
    img_list = img_list[num_test:]
    mask_list = mask_list[num_test:]

    train_x, valid_x, train_y, valid_y = train_test_split(img_list, mask_list, test_size=val_size,random_state=42)
    train_gen = BrainMRIs(batch_size, H,W, train_x, train_y)
    val_gen = BrainMRIs(batch_size, H, W, valid_x, valid_y)

    return train_gen, val_gen, test_list


