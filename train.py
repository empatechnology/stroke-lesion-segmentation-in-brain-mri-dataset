'''
Empa Technolgy is an intrapreneur investment of Empa Elektronik, which is the market leader in semiconductor sector.
We develop and integrate technology for the growing IoT (Internet of Things) and AI (Artificial Intelligence) market.
Our goal is create a real impact by designing and implementing solid, high-scale,
value-adding IoT and Industry 4.0 solutions for customers who are eager to catch up with digital transformation benefits.
Contact us to learn more (teknoloji@empa.com)

Author : Arda Burak Mamur
Title : Computer Vision and Machine Learning Engineer
Latest update : 16.02.2022
'''


from configparser import ConfigParser
from sklearn.model_selection import train_test_split
from src.functions import *
from src.models.custom_densenet import *
from src.models.double_unet import *
from src.models.loss import *
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
from tensorflow.keras.optimizers import *
from tensorflow.keras.metrics import Precision, Recall
import argparse

class Train:
    def __init__(self, task):
        config_object = ConfigParser()
        config_object.read("config.ini")
        info = config_object[task]

        ''' general parameters '''
        self.model_name = info['model']
        self.epoch = int(info['epochs'])
        self.batch_size = int(info['batch_size'])
        self.lr = float(info['learning_rate'])
        self.data_folder = info['data_path']
        self.val_size = float(info['val_size'])
        self.H = int(info['h'])
        self.W = int(info['w'])
        self.input_channel = int(info['input_channels'])
        self.n_classes = int(info['num_class'])

        ''' Specific parameters of tasks '''
        if task == "CLASSIFICATION":
            self.task = "cls"
            self.test_size = float(info['test_size'])
        elif task == "SEGMENTATION":
            self.task = "seg"
            self.num_test = int(info['num_test'])
            self.loss = info['loss']

    def get_cls_model(self,summary=True):
        model = densenet_model(self.model_name, n_classes=self.n_classes, height=self.H, width=self.W, channel_number=self.input_channel)
        metrics = ["accuracy"]
        loss = "categorical_crossentropy"
        optimizer = Adam(self.lr)
        model.compile(optimizer=optimizer, loss=loss, metrics=metrics)
        if summary:
            model.summary()
        return model

    def get_seg_model(self,summary=True):
        shape = (self.H, self.W, self.input_channel)
        print("model shape:", shape)
        model = double_unet(self.H, self.W, self.input_channel)
        metrics = [dice_coefficient, Precision(), Recall()]
        optimizer = Adam(self.lr)
        model.compile(loss=dice_loss, optimizer=optimizer, metrics=metrics)
        if summary:
            model.summary()
        return model


    def load_model(self):
        if self.task == "cls":
            model = self.get_cls_model(summary=True)
            return model
        elif self.task == "seg":
            model = self.get_seg_model(summary=True)
            return model

    def fit_model(self):
        model_dir = "models"
        cur_dir = os.getcwd()
        path = os.path.join(cur_dir, model_dir)
        if not os.path.exists(path):
            os.mkdir(path)
        best_model = "best_" + self.task + "_" + self.model_name + ".hdf5"
        best_model = path + "/" + best_model

        if self.task == "cls":
            train_img, val_img, train_labels, val_labels, train_steps, valid_steps = classification_preprocess(H=self.H,
                                                                                                               W=self.W,
                                                                                                               test_size=self.test_size,
                                                                                                               val_size=self.val_size,
                                                                                                               batch_size=self.batch_size)
            checkpoint = ModelCheckpoint(filepath=best_model, monitor='val_accuracy', save_best_only=True,
                                         save_weights_only=False, verbose=1)
            callback = [checkpoint]
            model = self.load_model()
            H = model.fit(x=train_img,
                          y=train_labels,
                          epochs=self.epoch,
                          validation_data=(val_img, val_labels),
                          steps_per_epoch=train_steps,
                          validation_steps=valid_steps,
                          callbacks=callback,
                          shuffle=False)

        elif self.task == "seg":
            train_data, val_data, test_list = segmentation_preprocess(H=self.H, W=self.W, val_size=self.val_size,
                                                                      num_test=self.num_test, batch_size=self.batch_size)
            reduce_lr = ReduceLROnPlateau(monitor="val_loss", factor=0.5, patience=5, mode='min', min_lr=1e-6,
                                          verbose=1)
            checkpoint = ModelCheckpoint(filepath=best_model, monitor='val_loss',
                                         save_best_only=True, mode='min', verbose=1)
            callback = [reduce_lr, checkpoint]
            model = self.load_model()
            H = model.fit(train_data,
                          validation_data=val_data,
                          shuffle=False,
                          epochs=self.epoch,
                          callbacks=callback)

        else:
            raise Exception("No task named as (" + self.task + ")")

    def train(self):
        create_csv(self.data_folder, self.H, self.W)
        self.fit_model()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--task', help="classification or segmentation", type=str, default='CLASSIFICATION')
    args, _ = parser.parse_known_args()
    if args.task == "CLASSIFICATION" or args.task == "SEGMENTATION":
        print("-----------------", args.task, " ----------------------")
        main = Train(args.task)
        main.train()
        print(" Training process is done...")
    else:
        raise Exception("No task named as ", args.task)

