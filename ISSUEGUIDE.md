## CREATE ISSUE REPORTING GUIDELINES
* Create an issue to report any bugs.
* Please set the parameter "Kind" as bug when reporting an issue.
* Please share screenshots, prints of an error you have. 
* Before creating an issue please have a look at other issues that may  you have similar solved problems.
