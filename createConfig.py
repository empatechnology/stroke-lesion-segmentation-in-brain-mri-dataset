'''
Empa Technolgy is an intrapreneur investment of Empa Elektronik, which is the market leader in semiconductor sector.
We develop and integrate technology for the growing IoT (Internet of Things) and AI (Artificial Intelligence) market.
Our goal is create a real impact by designing and implementing solid, high-scale,
value-adding IoT and Industry 4.0 solutions for customers who are eager to catch up with digital transformation benefits.
Contact us to learn more (teknoloji@empa.com)

Author : Arda Burak Mamur
Title : Computer Vision and Machine Learning Engineer
Latest update : 22.02.2022

'''
from configparser import ConfigParser

#Get the configparser object
config_object = ConfigParser()

#we have 2 different tasks which are classification and segmentation
config_object["CLASSIFICATION"] = {
    "model": "densenet169",
    "epochs": "100",
    "H": "512",
    "W": "512",
    "input_channels": "1",
    "learning_rate": "1e-3",
    "batch_size": "4",
    "num_class": "2",
    "val_size": "0.1",
    "test_size": "0.1",
    "data_path": "dataset"
}

config_object["SEGMENTATION"] = {
    "model": "densenet169",
    "epochs": "100",
    "H": "512",
    "W": "512",
    "input_channels": "1",
    "loss" : "dice_loss",
    "learning_rate": "1e-3",
    "optimizer": "Adam",
    "batch_size": "4",
    "num_class": "2",
    "val_size": "0.1",
    "num_test": "20",
    "data_path": "dataset"
}

#Write the above sections to config.ini file
with open('config.ini', 'w') as conf:
    config_object.write(conf)