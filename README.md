# Stroke Lesion Segmentation in Brain MRI Dataset

## Overview

The new research area has been analyzing photos and videos and exploiting them in diverse applications such as self-driving cars, drones, and other applications using deep learning algorithms. While these study areas are still focused on generic images, our goal is to apply medical image research to improve healthcare. Finally, we started our R&D project in the field of Medical Image Classification and Segmentation.

Regarding this project, we used MR Images as a dataset and we applied 2 different tasks to this dataset. The first one is the classification model which tries to classify a given MR brain image has a tumor or not. The second is the segmentation model. As you can understand from its name, this model tries to segment a problematic (tumor)  part of given brain images. 

![al text](https://media-exp1.licdn.com/dms/image/C4D12AQE5IyT6gMDFGA/article-inline_image-shrink_1500_2232/0/1619286967620?e=1648080000&v=beta&t=xSAKElCD-MrWk1jRutcxBdTL8uXB3j4WaOzrvR-qu1g)

## Quick Start

1. Install [Tensorflow](https://www.tensorflow.org/install)
2. Install dependencies
```bash
pip install -r requirements.txt
```
3. Download the [dataset](https://www.kaggle.com/mateuszbuda/lgg-mri-segmentation)
```
archive.zip
 └── kaggle_3m
 └── lgg-mri-segmentation
     └── kaggle_3m
```
  3. "kaggle_3m" and "lgg-mri-segmentation" folders are exactly the same!. 
      So you can move kaggle_3m folder to you project tree and please remove data.csv and README.md files from this folder. 
      Please do not forget to rename this folder to "dataset".




## Project Organization

```
root
 ├── dataset #Please rename the downloaded data folder("kaggle_3m") to "dataset"
 └── src
     └── models
         └── custom_densenet.py
         └── double_unet.py
         └── loss.py
     └── functions.py
 └── config.ini
 └── requirements.txt
 └── test.py
 └── train.py

```
## Training
You can edit inside the config.ini file regarding your preferences.
Then run the following command to start the training process.
```
> python3 train.py -h
  usage: --task [TASK]  [CLASSIFICATION, SEGMENTATION]

> python3 train.py --task CLASSIFICATION
```

## Evaluation & Prediction
Run the following command to get predictions.

```

> python3 test.py -h
  usage: --task [TASK]  [CLASSIFICATION, SEGMENTATION]
         --model [MODEL] [model path]
         --input [INPUT] [image path]

> python3 test.py --task CLASSIFICATION --model model.hdf5 --input example.tif

```

## License & Citation

Please advice the LICENSE.md file. For usage of third party libraries and repositories please advise the respective distributed terms. It would be nice to cite the original models and datasets. 
If you find this work useful, please cite as:

```
@software{empaAI_StrokeSegmentation,
author = {Bencik B., Mamur A., Ismailoglu I.},
title = {Stroke Lesion Segmentation in Brain MRI Dataset},
year = {2022},
note = "\url{https://bitbucket.org/empatechnology/stroke-lesion-segmentation-in-brain-mri-dataset/src/master/}"
organization = {Empa Technology}}
}

```